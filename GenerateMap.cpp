

/*******************************************************************************
** @author Nguyễn Đức Danh                                                    **
** Phần code sinh mê cung ngẫu nhiên theo chiều dài và chiều rộng cho trước   **
** Gồm 2 cách:                                                                **
** Cách 1: Random giá trị của 1 ô hoàn toàn ngẫu nhiên trong 0 và 1           **
** Cách 2: Sử dụng DFS                                                        **
********************************************************************************
** Cách dùng:                                                                 **
** include "data_type.h"                                                      **
** Gọi 1 trong 2 hàm:                                                         **
** 1. bool** generateMazeRandomly(int width, int height, int wallPercent)     **
** 2. bool** generateMazeDFS(int width, int height)                           **
** THANK YOU!                                                                 **
*******************************************************************************/

#include <iostream>
#include <vector>
#include <climits>
#include <cstdlib>
#include <ctime>

#include "data_type.h"


/* ================================================================= */

/* CÁCH 1: NGẪU NHIÊN HOÀN TOÀN */

/**
* Khởi tạo một mê cung hoàn toàn ngẫu nhiên
* @param width Chiều ngang
* @param height Chiều dọc
* @param wallPercent Tỉ lệ tường trong mê cung
*/
bool** generateMazeRandomly(int width, int height, int wallPercent)
{
    bool** mazeMap;

//  Chuẩn hóa giá trị wallPercent
    if (wallPercent < 0) wallPercent = 0;
    if (wallPercent > 100) wallPercent = 99;

    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    srand((time_t)ts.tv_nsec);

//  Cấp phát bộ nhớ và khởi tạo giá trị ban đầu
    mazeMap = init(width, height, false);

    for (int i = 0; i < height; i++)
        for (int j = 0; j < width; j++)
        {
//          Quyết định 1 ô là tường hay là đường một cách ngẫu nhiên
            int tmp = rand() % 100;

            if (tmp < wallPercent) mazeMap[i][j] = false;
            else mazeMap[i][j] = true;
        }

//  Ô bắt đầu và kết thúc luôn là true
    mazeMap[0][0] = true;
    mazeMap[height-1][width-1] = true;

    return mazeMap;
}

/* ================================================================= */

/* CÁCH 2: SỬ DỤNG DFS */

/**
* Hàm đổi chỗ 2 phần tử trong mảng ngẫu nhiên
*/
void swap(int* number_1, int* number_2)
{
	int temp = *number_1;
	*number_1 = *number_2;
	*number_2 = temp;
}

/**
* Hàm tạo mảng 4 phần tử để tìm kiếm đường đi trong ma trận một cách ngẫu nhiên
* @param arr con trỏ mảng
* @param n kích thước mảng
*/
void shuffleArray(int* arr, int n)
{
	struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    srand((time_t)ts.tv_nsec);

    // Vị trí nhỏ nhất chưa đổi chỗ
	int minPosition;

	// Vị trí sau cùng
	int maxPosition = n - 1;

	// Vị trí cần đổi chỗ hiện tại
	int swapPosition;

	int i = 0;
	while (i < n - 1)
	{
		minPosition = i + 1;

		//chọn 1 phần tử để đổi chỗ
		swapPosition = rand() % (maxPosition - minPosition + 1) + minPosition;

        // Đổi chỗ phần tử cần đổi chỗ và ví trí hiện tại i
		swap(&arr[i], &arr[swapPosition]);
		i++;
	}
}

/**
* DFS đệ quy để duyệt hết các phần tử có (m*n%2=1) trong mảng
*/
void generateMazeDFS_recursion(bool** maze, int r, int c, int height, int width)
{
// Sinh mang xáo trộn ngẫu nhiên có 4 phần tử {1, 2, 3, 4}

    int rddir[4] = {1, 2, 3, 4};
    shuffleArray(rddir, 4);
// Duyệt hết các phần tử trong mảng để duyệt hết cả 4 đỉnh xung quanh điểm hiện tại
// Đưa ra lần lượt các hướng đi
    for (int i=0; i<4; i++)
    {
        switch(rddir[i]){
        // Lên trên
        case 1:
            if ((r + 2) >= height)
                continue;
            if (maze[r+2][c] == !1)
            {
                maze[r+2][c] = 1;
                maze[r+1][c] = 1;
                // printf(" 1");
                generateMazeDFS_recursion(maze, r+2, c, height, width);
                continue;

            }
        // Sang phải
        case 2:
            if ((c + 2) >= width)
                continue;
            if (maze[r][c+2] == !1)
            {
                maze[r][c+2] = 1;
                maze[r][c+1] = 1;
                // printf(" 1");
                generateMazeDFS_recursion(maze, r, c+2, height, width);

                continue;
            }
        // Đi xuống
        case 3:
            if ((r - 2) < 0)
                continue;
            if (maze[r-2][c] == !1)
            {
                maze[r-2][c] = 1;
                maze[r-1][c] = 1;
                // printf(" 1");
                generateMazeDFS_recursion(maze, r-2, c, height, width);

                continue;
            }
         // Sang trái
         case 4:
            if ((c - 2) < 0)
                continue;
            if (maze[r][c-2] == !1)
            {
                maze[r][c-2] = 1;
                maze[r][c-1] = 1;
                // printf(" 1");
                generateMazeDFS_recursion(maze, r, c-2, height, width);

                continue;
            }
        }
    }
}


bool** generateMazeDFS(int width, int height)
{
//  Mảng trả về
    bool** mazeMap;
//  Tọa độ ô bắt đầu sinh ma trân
    int r, c, d=0;

    mazeMap = generateMazeRandomly(width, height, 90);

    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    srand((time_t)ts.tv_nsec);
     r = rand() % height;
     while (r % 2 == 0) {
         r = rand() % height;
     }
     // srand (time(NULL));
     c = rand() % width;
     while (c % 2 == 0) {
         c = rand() % width;
     }
     //
     mazeMap[r][c] = true;


    generateMazeDFS_recursion(mazeMap, r, c, height, width);
    for (int i = 0; i < height; i++)
        for (int j = 0; j < width; j++)
        {
//          Quyết định 1 ô là tường hay là đường một cách ngẫu nhiên
            int tmp = rand() % 100;

            if (tmp < 100) mazeMap[i][j] ;
            else mazeMap[i][j] = true;
        }


    mazeMap[0][1] = mazeMap[1][0] = mazeMap[1][1] = true;
    mazeMap[height-1][width-2] = mazeMap[height-2][width-1] = mazeMap[height-2][width-2] = true;

    return mazeMap;
}
