#include <iostream>
#include <stdio.h>
#include <fstream>
#include <time.h>
#include <stdlib.h>
#define MAX 120
using namespace std;
/*
* Tạo mảng maze
*/
bool maze[MAX][MAX];
void randomNode(int height, int width);
/*
* Hàm đổi chỗ 2 phần tử trong mảng ngẫu nhiên
*/

void Swap(int* number_1, int* number_2)
{
	int temp = *number_1;
	*number_1 = *number_2;
	*number_2 = temp;
}
/*
*Hàm tạo mảng 4 phần tử để tìm kiếm đường đi trong ma trận một cách ngẫu nhiên
*/
void ShuffleArray(int* arr, int n)
{
	// srand(time(NULL));
    // Vị trí nhỏ nhất chưa đổi chỗ
	int minPosition;
	// Vị trí sau cùng
	int maxPosition = n - 1;
	// Vị trí cần đổi chỗ hiện tại
	int swapPosition;

	int i = 0;
	while (i < n - 1)
	{
		minPosition = i + 1;
		//chọn 1 phần tử để đổi chỗ
		swapPosition = rand() % (maxPosition - minPosition + 1) + minPosition;
        // Đổi chỗ phần tử cần đổi chỗ và ví trí hiện tại i
		Swap(&arr[i], &arr[swapPosition]);
		i++;
	}
}
/*
* Tạo ra một mảng gồm các các số 0, coi như đây là bức tường
*/
void ganerateMaze(int height, int width, int &c, int &r)
{
    for (int i = 0; i < height; i++)
        for (int j = 0; j < width; j++)
            maze[i][j] = 0;
     srand (time(NULL));
     r = rand() % height;
     while (r % 2 == 1) {
         r = rand() % height;
     }
     // srand (time(NULL));
     c = rand() % width;
     while (c % 2 == 1) {
         c = rand() % width;
     }
     //
     maze[r][c] = 1;
}
/*
* DFS đệ quy để duyệt hết các phần tử có (m*n%2=1) trong mảng
*/
void recursion(int r, int c, int height, int width)
{
// Sinh mang xáo trộn ngẫu nhiên có 4 phần tử {1, 2, 3, 4}

    int rddir[4] = {1, 2, 3, 4};
    ShuffleArray(rddir, 4);
// Duyệt hết các phần tử trong mảng để duyệt hết cả 4 đỉnh xung quanh điểm hiện tại
// Đưa ra lần lượt các hướng đi
    for (int i=0; i<4; i++)
    {
        switch(rddir[i]){
        // Lên trên
        case 1:
            if ((r + 2) >= height)
                continue;
            if (maze[r+2][c] == !1)
            {
                maze[r+2][c] = 1;
                maze[r+1][c] = 1;
                // printf(" 1");
                recursion(r+2, c, height, width);
                continue;

            }
        // Sang phải
        case 2:
            if ((c + 2) >= width)
                continue;
            if (maze[r][c+2] == !1)
            {
                maze[r][c+2] = 1;
                maze[r][c+1] = 1;
                // printf(" 1");
                recursion(r, c+2, height, width);

                continue;
            }
        // Đi xuống
        case 3:
            if ((r - 2) < 0)
                continue;
            if (maze[r-2][c] == !1)
            {
                maze[r-2][c] = 1;
                maze[r-1][c] = 1;
                // printf(" 1");
                recursion(r-2, c, height, width);

                continue;
            }
         // Sang trái
         case 4:
            if ((c - 2) < 0)
                continue;
            if (maze[r][c-2] == !1)
            {
                maze[r][c-2] = 1;
                maze[r][c-1] = 1;
                // printf(" 1");
                recursion(r, c-2, height, width);

                continue;
            }
        }
    }
}
/*
* Tạo thêm đường đi bằng cách chuyển ngẫu nhiên 1 số bit 0 thành 1
*/
void randomNode(int height, int width)
{
    int d = 0;
    int r, c;
    while (d < 10)
    {
// Chọn điểm có tọa độ lẻ (hiện tai đang là tường).
        r = rand() % height;
            while (r % 2 == 0) {
            r = rand() % height + 1;
     }
        c = rand() % width;
            while (c % 2 == 0) {
            c = rand() % width + 1;
     }
        if (maze[r][c] == 1) continue;
        if(((maze[r+1][c] == 0) && (maze[r-1][c] == 0) && (maze[r][c+1]) && (maze[r][c-1] == 1))
           || ((maze[r+1][c] == 1) && (maze[r-1][c] == 1) && (maze[r][c+1] ==0) && (maze[r][c-1] == 0)))
        {
            maze[r][c] = 1;
            d++;
            cout << r << " " << c << endl;
        }
    }
}
/*
* Đưa ra mê cung
*/
void printMaze(int height, int width)
{
    for (int i=0; i<height; i++)
    {
      for (int j=0; j<width; j++)
        cout <<" "<<maze[i][j];
    cout << " "<<endl;
    }
}

/*int main()
{
    int height = 15;
    int width = 15;
    int r, c;
    // int rddir[4]={1, 2, 3, 4};
    ganerateMaze(height,width,c,r);
// Vị trí của điểm bắt đầu tạo mê cung
    cout << r << " "<< c << endl;
    recursion(r,c,height,width);
    randomNode(height,width);
    printMaze(height,width);
}*/
