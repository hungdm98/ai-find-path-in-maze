

/*******************************************************************************
** @author Đinh Mạnh Hùng                                                     **
** Phần code tìm đường đi bằng BFS (Dùng hàng đợi)                            **
**
** Cách dùng:                                                                 **
** include "data_type.h"                                                      **
** Gọi hàm bool bfs (bool** mazeMap, int width, int height, coordinate startPos, coordinate goalPos)                   **
** Hàm trả về 0 nếu không tìm thấy đường và 1 nếu tìm thấy đường              **
** Thông tin chi tiết đường đi được lưu trong vector path và visitHistory     **
** THANK YOU!                                                                 **
*******************************************************************************/


#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>
#include <queue>


#include "data_type.h"


extern vector <coordinate> path;   // Lưu đường đi

extern vector <coordinate> visitHistory; // Lưu lịch sử thăm

bool** visited; // Mảng 2 chiều, đánh dấu 1 điểm đã được đến thăm hay chưa

extern coordinate** previousCell; // Điểm trước đó dẫn đến điểm này



/**
* Tìm đường đi trong mê cung bằng BFS
* @return true nếu tìm được đường và ngược lại
* @param mazeMap Mảng 2 chiều lưu thông tin mê cung
* @param width Chiều ngang mê cung
* @param height Chiều dọc
*/
bool bfs (bool** mazeMap, int width, int height, coordinate startPos, coordinate goalPos)
{
    // Khởi tạo giá trị
    visited = init(width, height, false);
    previousCell = init(width, height, make_pair(-1, -1));

    // Hàng đợi chứa tọa độ các nút được thăm
    queue<coordinate> qList;

    // Đưa điể m xuất phát vào hàng đợi và đánh dấu nó là đã thăm
    visited[startPos.first][startPos.second] = true;
    qList.push(startPos);

    bool stop = false;

    // Lặp cho đến khi không còn phần tử nào trong hàng đợi
    while (!qList.empty())
    {
        // Lấy tọa độ điểm đầuu tiền từ trong hàng đợi ra
        coordinate currentPos = qList.front();
        qList.pop();

        if (currentPos == goalPos) stop = true;
        // Thêm nó vào lịch sử duyệt
        if(!visited[currentPos.first][currentPos.second] && !stop)
            visitHistory.push_back(currentPos);


        // Do BFS không cải thiện chi phí sau mõi lần duyệt
        // Nên ta đánh dấu điểm đó là đã thăm để tránh xét lại
        visited[currentPos.first][currentPos.second] = true;

        // Kiểm tra các điểm bên trên
        if (currentPos.first > 0)
        {
            coordinate nextPos = make_pair(currentPos.first - 1, currentPos.second);

            // Nếu như điểm đó không phải tường và chưa được thăm
            // Ta thêm nó vào hàng đợi và đánh dấu điểm cha của nó
            if (!visited[nextPos.first][nextPos.second] && mazeMap[nextPos.first][nextPos.second])
            {
                previousCell[nextPos.first][nextPos.second] = currentPos;
                qList.push(nextPos);
            }
        }

        // Kiểm tra các điểm bên dưới
        if (currentPos.first < height-1)
        {
            coordinate nextPos = make_pair(currentPos.first + 1, currentPos.second);

            // Nếu như điểm đó không phải tường và chưa được thăm
            // Ta thêm nó vào hàng đợi và đánh dấu điểm cha của nó
            if (!visited[nextPos.first][nextPos.second] && mazeMap[nextPos.first][nextPos.second])
            {
                previousCell[nextPos.first][nextPos.second] = currentPos;
                qList.push(nextPos);
            }
        }

        // Kiểm tra các điểm bên trai
        if (currentPos.second > 0)
        {
            coordinate nextPos = make_pair(currentPos.first, currentPos.second - 1);

            // Nếu như điểm đó không phải tường và chưa được thăm
            // Ta thêm nó vào hàng đợi và đánh dấu điểm cha của nó
            if (!visited[nextPos.first][nextPos.second] && mazeMap[nextPos.first][nextPos.second])
            {
                previousCell[nextPos.first][nextPos.second] = currentPos;
                qList.push(nextPos);
            }
        }

        // Kiểm tra các điểm bên phai
        if (currentPos.second < width -1 )
        {
            coordinate nextPos = make_pair(currentPos.first, currentPos.second + 1);

            // Nếu như điểm đó không phải tường và chưa được thăm
            // Ta thêm nó vào hàng đợi và đánh dấu điểm cha của nó
            if (!visited[nextPos.first][nextPos.second] && mazeMap[nextPos.first][nextPos.second])
            {
                previousCell[nextPos.first][nextPos.second] = currentPos;
                qList.push(nextPos);
            }
        }
    }


    //  Kiểm tra có tìm được đường hay khoogn
    if (previousCell[goalPos.first][goalPos.second] == make_pair(-1, -1))
    {
        delete[] visited;
        delete[] previousCell;
        return false;
    }

    else
    {
//      Lấy thông tin đường đi
        coordinate tmp = goalPos;

        path.push_back(tmp);

        while (previousCell[tmp.first][tmp.second] != make_pair(-1, -1))
        {
            tmp = previousCell[tmp.first][tmp.second];
            path.push_back(tmp);
        }

//      Đường đi lưu ngược nên phải reverse lại
        reverse(path.begin(), path.end());
    }

        delete[] visited;
        delete[] previousCell;
        return true;
}
