﻿╔══════════════════════════════════════════╗
║****** BÀI TẬP LỚN TRÍ TUỆ NHÂN TẠO ******║
║*******           NHÓM 16          *******║
╠══════════════════════════════════════════╣
║** Hoàng Thị Hồng 		20155668 **║
║** Nguyễn Đức Danh		20160600 **║
║** Đinh Mạnh Hùng		20161925 **║
║** Đinh Hoàng Nam		20162793 **║
╚══════════════════════════════════════════╝

File này gồm có:
1. Cấu trúc project
2. Hướng dẫn cài đặt trên Windows
3. Hướng dẫn cài đặt trên Linux Ubuntu




================== 1. CẤU TRÚC PROJECT ==================

─ Chương trình được viết trên ngôn ngữ C++, sử dụng thư viện openCV để hiển thị giao diện đồ hoạ
─ Bao gồm các file:
  + FindPathinMaze.cbp: File codeblocks project để mở bằng IDE Codeblocks
  + data_type.h, Astarv3.h: Các file header
  + main.cpp: Chương trình chính
  + GenerateRandomMap.cpp: Chứa các hàm liên quan đến khởi tạo và sinh mê cung ngẫu nhiên
  + DFS.cpp: Cài đặt huật toán DFS
  + BFS.cpp: Cài đặt thuật toán BFS
  + AStarv3.cpp: Cài đặt thuật toán A*
  + GUI.cpp: Chứa các hàm hiển thị mê cung ra giao diện đồ hoạ, sử dụng thư viện openCV

================== 2. HƯỚNG DẪN CÀI ĐẶT TRÊN WINDOWS ==================

I. Download
1. download OpenCV 3.2:
https://sourceforge.net/projects/opencvlibrary/files/opencv-win/3.2.0/opencv-3.2.0-vc14.exe/download
 
2. download Cmake: 
https://github.com/Kitware/CMake/releases/download/v3.13.1/cmake-3.13.1-win64-x64.zip

3. download TDM-GCC
https://sourceforge.net/projects/tdm-gcc/files/TDM-GCC%205%20series/5.1.0-tdm64-1/gcc-5.1.0-tdm64-1-core.zip/download

4. download CodeBlock: (without mingw) và lưu tại F:\AI-Setup
https://sourceforge.net/projects/codeblocks/files/Binaries/17.12/Windows/codeblocks-17.12-setup.exe/download

II. Setup
1. chạy opencv-3.2.0-vc14.exe -> được thư mục "opencv"
2. giải nén cmake-3.13.1-win64-x64.zip -> được thư mục "cmake-3.13.1-win64-x64"
3. giải nén gcc-5.1.0-tdm64-1-core.zip 
4. chạy codeblocks-17.12-setup.exe -> nhấn next..... finish

III. Set SystemPath
1. Nhấn tổ hợp phím window và pause/break hoặc chuột phải vào mycomputer và chọn Properties
2. Nhấn Advanced system settings (bên lề trái)
3. Nhấn Enviroment Variables
4. Ở phần System variables double click vào dòng có tên Path
5. Từ cửa sổ hiện ra chọn New
6. paste F:\AI-Setup\tdm\bin
7. tiếp tục paste: F:\AI-Setup\cmake-3.13.1-win64-x64\bin

IV. Making binaries
1. Chạy cmake-gui.exe trong thư mục: F:\AI-Setup\cmake-3.13.1-win64-x64\bin

2. 
Browse Source: F:/AI-Setup/opencv/sources
Browse Build: F:\AI-Setup\opencv\build

3.
Hit configure button and from the drop-down menu select ‘codeblocks – MinGW Makefiles’ and press finish.

......chờ một chút để chương trình chạy (khoảng 2-5 phút)
......chương trình báo Configuring done

Nhấn Generate
......chương trình báo Generating done

4.
Double click vào file OpenCV.cbp trong thư mục: F:\AI-Setup\opencv\build
Chọn mở bằng CodeBlock

Go to ‘Settings‘, choose ‘Compiler’ and click ‘Toolchain executable‘. In the ‘compiler’s installation directory‘ field choose the “bin” folder of MinGW F:\AI-Setup\tdm\bin.

Set the following:
c compile: gcc.exe

c++ compiler: g++.exe

Linker for dynamic libs: ar.exe

Go to ‘build ->select target -> install’
Hit Build buttuon

....chờ rất lâu, tùy cấu hình từng máy, có thể mất từ 30,45 phút -> 2,3 giờ

....chương trình thông báo === Build finished: 0 error(s), 2 warning(s), (32 minute(s), 53 second(s)) ===

Tiếp tục set path: F:\AI-Setup\opencv\build\install\x64\mingw\bin giống như phần II đã làm.


V. Running test program

Create a C++ project ‘Test’ in codeblocks.

Go to settings -> compiler.

=====
Select ‘search directories’ and in the ‘compiler’ tab chose the followings:

F:\AI-Setup\opencv\build\install\include

F:\AI-Setup\opencv\build\install\include\opencv

F:\AI-Setup\opencv\build\install\include\opencv2

Select ‘Linker’ tab and add 
F:\AI-Setup\opencv\build\install\x64\mingw\lib


======
Under ‘Linker Settings’ tab add required libraries 

F:\AI-Setup\opencv\build\install\x64\mingw\lib\lib*.dll.a


======
Thêm 1 file ảnh bất kì với tên đặt là "pic.jpg" vào gốc của thư mục project "Test"


======
thêm đoạn code sau đây vào file main.cpp và chạy chương trình để xem kết quả

#include "core.hpp";
#include "highgui.hpp"
#include "imgcodecs.hpp"

using namespace cv;
using namespace std;

int main()
{
Mat img;
img = imread("pic.jpg");
imshow("Original Image", img);
waitKey();
}


=============
nếu thực hiện theo từng bước như trên thì khả năng thành công là rất cao (99,99%) 

link ảnh tham khảo các bước: https://drive.google.com/open?id=1qhyYjjmXVWL0JPV6D2JYHZmokKLl5ai0



================== 3. HƯỚNG DẪN CÀI ĐẶT TRÊN LINUX UBUNTU ==================


1. Cài đặt Codeblocks IDE:
$ sudo apt-get install codeblocks

2. Cài đặt trình biên dịch C/C++:
$ sudo apt-get install build-essential

3. Cài đặt thư viện openCV:
$ sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
$ sudo apt-get install libopencv-dev

4. Add project vào Codeblocks:
Mở trực tiếp file FindPathinMaze.cbp hoặc mở Codeblocks, chọn Open -> rồi chọn file FindPathinMaze.cbp

5. Liên kêt tới thư viện openCV:
Trên giao diện của codeblocks, chọn Project -> Build Option. Tại thẻ Compiler Option, chọn Other Options.
Rồi thêm các dòng sau:
╔══════════════════════════════════════════╗
║ -s					   ║
║ `pkg-config --cflags opencv`		   ║
╚══════════════════════════════════════════╝
Chọn thẻ Linker Settings, tại mục Other linker options, thêm các dòng sau:
╔══════════════════════════════════════════╗
║ `pkg-config --libs opencv`		   ║
╚══════════════════════════════════════════╝

6. Chạy chương trình bằng cách nhấn F9

Video hướng dẫn: https://www.youtube.com/watch?v=30Qioj-5pG0
