#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <chrono>


#include "data_type.h"
#include "AStarv3.h"

using namespace std;

vector <coordinate> path;   // Lưu đường đi

vector <coordinate> visitHistory; // Lưu lịch sử thăm




/**
* Lựa chọn thuật toán
*/
void setAlgorithm(int &algorithm, int &heuristicFunc, int &alpha, int &beta)
{
    // LỰA CHỌN THUẬT TOÁN
    cout << "Select an algorithm to solve the maze:" << endl;
    cout << "1. Deep first search" << endl;
    cout << "2. Breadth first search" << endl;
    cout << "3. Greedy algorithm" << endl;
    cout << "4. A star algorithm" << endl;

    cin >> algorithm;

    if (algorithm == 4|| algorithm == 3) {
        cout << "Select heuristic function: " << endl;
        cout << "1. Euclidian Distance" << endl;
        cout << "2. Manhattan Distance" << endl;
        cin >> heuristicFunc;
    }
    if (algorithm == 4) {
        cout << "Enter scale of greedy function g(x)" <<endl<<"alpha = ";
        cin >> alpha;
        cout << "Enter scale of heuristic function h(x)" << endl << "beta = ";
        cin >> beta;

    }
}



/**
* Kiểm tra 1 tọa độ có phải là ô trong mê cung hay ko
*/
bool isCell (bool** maze, int width, int height, int x, int y)
{
    if (x >= width) return false;
    if (y >= height) return false;
    return maze[y][x];
}

/**
* In kq ra man hinh console
*/
void printPath (vector<coordinate> path)
{
    int n = path.size();
    for (int i=0; i < n; i++) {
        cout << "(" << path[i].first << ", " << path[i].second << "), ";
    }
    cout << endl;
}

int main()
{

    int width, height;  // Chiều dài và chiều rộng của mê cung
    bool** maze;        // Mảng 2 chiều lưu trạng thái của mê cung
    int algorithm;      // Thuật toán được lựa chọn
    int alpha, beta;    // Ti le giua h va g trong A*
    int heuristicFunc;  // Lua chon ham h1 hay h2 trong A*
    int startX, startY, goalX, goalY;
    int cont = 1; // Tiếp tục test hay là dừng
    long duration; // Đo thời gian chạy của thuật toán
    cv::Mat map1; // Để vẽ ma trận

    cout << "Maze width = ";
    cin >> width;
    cout << "Maze height = ";
    cin >> height;

     // Sinh ma trận bằng DFS
     // true là có đường đi
     // false là tường
    maze = generateMazeDFS(width, height);

    // HHiển thị mê cung ra màn hình
    map1 = drawMaze("map 1", width, height, maze);



    // chọn tọa độ bắt đầu
    do {
        cout << "Start position (x, y):" << endl;
        cout << "x = ";
        cin >> startX;
        cout << "y = ";
        cin >> startY;
        if (!isCell(maze, width, height, startX, startY)) {
            cout << "This is a wall" << endl;
        }
    } while (!isCell(maze, width, height, startX, startY));
    markPos(map1, "map 1", width, height, startX, startY);


     // chọn tọa độ bắt đầu
    do {
        cout << "Goal position (x, y):" << endl;
        cout << "x = ";
        cin >> goalX;
        cout << "y = ";
        cin >> goalY;
        if (!isCell(maze, width, height, goalX, goalY)) {
            cout << "This is a wall" << endl;
        }
    } while (!isCell(maze, width, height, goalX, goalY));
    markPos(map1, "map 1", width, height, goalX, goalY);

    while (cont) {

        path.clear();
        visitHistory.clear();

        setAlgorithm(algorithm, heuristicFunc, alpha, beta);
        // Đánh dấu thời gian bắt đầu
        std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        switch (algorithm) {
            case 1: dfs(maze, width, height, make_pair(startY, startX), make_pair(goalY, goalX)); break;
            case 2: bfs(maze, width, height, make_pair(startY, startX), make_pair(goalY, goalX)); break;
            case 3: AStart(maze, height, width, startY, startX, goalY, goalX, 0, 1, heuristicFunc); break;
            case 4: AStart(maze, height, width, startY, startX, goalY, goalX, alpha, beta, heuristicFunc); break;
        }
        // Đánh dấu thời gian kết thúc
        std::chrono::steady_clock::time_point end= std::chrono::steady_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();

        drawPath(map1, width, height, visitHistory, path);

        cout << "Visit history: " << endl;
        printPath(visitHistory);
        cout << "Solution found: " << endl;
        printPath(path);


        cout << "--- RESULT ---" << endl;
        cout << "Total cell visited: " << visitHistory.size() << endl;
        cout << "Path length: " << path.size() << endl;
        cout << "Runtime: " << duration << " microseconds" << endl;

        cout << "Do you want to continue? (1 = Yes, 0 = No)"<<endl;
        cin >> cont;

        if (cont)
        {
            map1 = drawMaze("map 1", width, height, maze);
            markPos(map1, "map 1", width, height, startX, startY);
            markPos(map1, "map 1", width, height, goalX, goalY);
        }
    }
    cout << "Good bye!";
    delete[] maze;
}
