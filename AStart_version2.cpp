/*
AStart find path in maze:

b1: put initial state into priority_queue (pq)
b2:
do
{
	cur <-- pq.top(); // cur - current state
	mark[cur] = used;
	if (cur == goal)
		return solotion;
	if not
		try to expand Up, Down, Left, Right from cur,
		then put them into pq.

}while( pq.empty() == false );

b3: if b2 return a solution, then trace its path.
	else return failure

*/

/*
To use this function --> #include "AStart_version2.h"
then call AStart() fucntion:

void AStart(bool** a, int m, int n, int x1, int y1, int x2, int y2, int option = 2, bool verboseFlag = false)

Input:
bool**a : maxtrix
int m : number of row
int n : number of col
int x1 : row of initial start node
int y1 : col of initial start node
int x2, y2: row and col of goal node
int option : <1> for use h1x(), <2> for use h2x()
bool verboseFlag: <true> for more detail when run console

Output:
Path from start --> goal on console.
*/

/*
this code not optimize yet, but it run, so I leave it as version2
in future, maybe need to do some more things like:
- add a function to free the dynamic allocated memory
- improve trace() function
- arrange all the code, make it cleaner, and optimize it.
- use graphic to more attractive.
*/

/*
- Tell me if you find something wrong, (y)!
*/

#include <stdio.h>
#include <iostream>
#include <queue>
#include <stack>
#include <vector>
#include <cmath>
#include <string>

#include "AStart_version2.h"
using namespace std;


// for short naming
typedef priority_queue<NodeS, vector<NodeS>, MyComparator1> pq_type1;
typedef priority_queue<NodeS, vector<NodeS>, MyComparator2> pq_type2;


// caculate g(x) cost
int gx(NodeS p)
{
	if(p == NULL)
		return 0;
	else
	{
		NodeS parent = p->parent;
		if(parent == NULL)
			return 1;
		else
			return parent->g + 1; // cost for each step is 1;
	}
}


// h1(x)
float h1x(NodeS p, NodeS goal)
{
	return sqrt( (p->x - goal->x)*(p->x - goal->x) + (p->y - goal->y)*(p->y - goal->y) );
}

// h2(x)
int h2x(NodeS p, NodeS goal)
{
	return ( abs(p->x - goal->x) + abs(p->y - goal->y) );
}


// make a node with its coordinate, other fields is default
NodeS makeNode(int x, int y)
{
	NodeS temp = new Node;
	temp->x = x;
	temp->y = y;
	temp->parent = NULL;
	temp->action = "";
	temp->g = 0;
	temp->h2 = 0;
	temp->f2= 0;
	temp->h1 = 0.0;
	temp->f1 = 0.0;

	return temp;
}


// make initial node
NodeS makeStartNode(int x, int y, NodeS goal)
{
	NodeS start = makeNode(x, y);
	start->parent = NULL;
	start->action = "Start:";
	start->g = 0;
	start->h1 = h1x(start, goal);
	start->h2 = h2x(start, goal);
	start->f1 = start->g + start->h1;
	start->f2 = start->g + start->h2;

	return start;
}


// make target node
NodeS makeGoalNode(int x, int y)
{
	NodeS goal = makeNode(x, y);
	goal->action = "Goal";
	return goal;

}


//check if reach goal
bool checkGoal(NodeS p, NodeS goal)
{
	return ( p->x == goal->x && p->y == goal->y );
}


// caculate costs by call to g(x), h1(x), h2(x)
void caculateCost(NodeS p, NodeS goal)
{
	p->g = gx(p);
	p->h1 = h1x(p, goal);
	p->h2 = h2x(p, goal);
	p->f1 = p->g + p->h1;
	p->f2 = p->g + p->h2;
}

// print some information about a node
void printNode(NodeS p, int option, bool printMore = true)
{
	if(printMore)
	{
		if( option == 2 )
			cout <<"\n" << p->action << " --> " << " <"  << p->x << ", " << p->y << "> with fx2 = " << p->f2;
		else if( option == 1)
			cout << "\n" << p->action << " --> " << " <" << p->x << ", " << p->y << "> with fx1 = " << p->f1;
	}
	else
	{
		cout << " <" << p->x << ", " << p->y << ">";
	}
}

// try to expand up from node p
void expandNodeU(NodeS p, int m, int n, bool** a, bool** close, NodeS goal, pq_type1& pq1, pq_type2& pq2, int option = 2, bool verboseFlag = false )
{
	// check if valid
	if( p->x == 0 || a[p->x -1][p->y] == 0 || close[p->x -1][p->y] == true )
		return;

	NodeS newNode = makeNode(p->x - 1, p->y);
	newNode->parent = p;
	newNode->action = "U";
	caculateCost(newNode, goal);

	pq1.push(newNode);
	pq2.push(newNode);
	if(verboseFlag)
		printNode(newNode, option);
}


// try to expand down from node p
void expandNodeD(NodeS p, int m, int n, bool** a, bool** close, NodeS goal, pq_type1& pq1, pq_type2& pq2, int option = 2, bool verboseFlag = false )
{
	// check if valid
	if( p->x == m-1 || a[p->x +1][p->y] == 0 || close[p->x +1][p->y] == true )
		return;

	NodeS newNode = makeNode(p->x + 1, p->y);
	newNode->parent = p;
	newNode->action = "D";
	caculateCost(newNode, goal);

	pq1.push(newNode);
	pq2.push(newNode);
	if(verboseFlag)
		printNode(newNode, option);
}

// ...
void expandNodeL(NodeS p, int m, int n, bool** a, bool** close, NodeS goal, pq_type1& pq1, pq_type2& pq2, int option = 2, bool verboseFlag = false )
{
	if( p->y == 0 || a[p->x][p->y - 1] == 0 || close[p->x][p->y - 1] == true )
		return;

	NodeS newNode = makeNode(p->x, p->y - 1);
	newNode->parent = p;
	newNode->action = "L";
	caculateCost(newNode, goal);

	pq1.push(newNode);
	pq2.push(newNode);
	if(verboseFlag)
		printNode(newNode, option);
}

// ...
void expandNodeR(NodeS p, int m, int n, bool** a, bool** close, NodeS goal, pq_type1& pq1, pq_type2& pq2, int option = 2, bool verboseFlag = false )
{
	if( p->y == n-1 || a[p->x][p->y + 1] == 0 || close[p->x][p->y + 1] == true )
		return;

	NodeS newNode = makeNode(p->x, p->y + 1);
	newNode->parent = p;
	newNode->action = "R";
	caculateCost(newNode, goal);

	pq1.push(newNode);
	pq2.push(newNode);
	if(verboseFlag)
		printNode(newNode, option);
}


// for short calling
void expandNode(NodeS p, int m, int n, bool** a, bool** close, NodeS goal, pq_type1& pq1, pq_type2& pq2, int option = 2, bool verboseFlag = false )
{
	expandNodeU(p, m, n, a, close, goal, pq1, pq2, option, verboseFlag);
	expandNodeD(p, m, n, a, close, goal, pq1, pq2, option, verboseFlag);
	expandNodeL(p, m, n, a, close, goal, pq1, pq2, option, verboseFlag);
	expandNodeR(p, m, n, a, close, goal, pq1, pq2, option, verboseFlag);
}


// whether print detail infomation when run console
void verbose(NodeS cur, int& c, int option)
{
    cout << "\n\n\n--------> Lan thu: " << ++c << " <--------" << endl;
    cout << "Choose node: ";
    printNode(cur, option, false);
    cout << "\nExpand:";
}


// this function call to others function above
NodeS solution(NodeS start, NodeS reach, int m, int n, bool** a, bool** close, NodeS goal, pq_type1& pq1, pq_type2& pq2, int option = 2, bool verboseFlag = false) // option 1 --> h1(); option2 --> h2()
{
	if( option == 1 )
	{
		pq1.push(start);
		int c = 0;

		while( !pq1.empty() )
		{
		    NodeS cur = pq1.top();
			pq1.pop();
			close[cur->x][cur->y] = true;

			if(verboseFlag)
                verbose(cur, c, option);

			if( checkGoal(cur, goal) )
			{
				reach = cur;
				return reach;
			}

			else
			{
				expandNode(cur, m, n, a, close, goal, pq1, pq2, option, verboseFlag);
			}
		}

		return NULL;
	}

	else if( option == 2 )
	{
		pq2.push(start);
		int c = 0;

		while( !pq2.empty() )
		{
		    NodeS cur = pq2.top();
			pq2.pop();
			close[cur->x][cur->y] = true;

			if(verboseFlag)
                verbose(cur, c, option);

			if( checkGoal(cur, goal) )
			{
				reach = cur;
				return reach;

			}

			else
			{
				expandNode(cur, m, n, a, close, goal, pq1, pq2, option, verboseFlag);
			}
		}

		return NULL;
	}
}


// to find path
void trace(NodeS reach)
{
	if(reach == NULL)
	{
		cout << "\nfailure";
	}

	else
	{
		//trace path
		stack<NodeS> S;
		NodeS it = reach;

		while( it != NULL)
		{
			S.push(it);
			it = it->parent;
		}

		//
		stack<NodeS> S1 = S;

		// print path
		cout << "\n\n\n*********************Solution:********************* \n";
		cout << "Start:";

		while( !S.empty() )
		{
			NodeS it = S.top(); S.pop();
			cout << "<" << it->x << "," << it->y << "> --> ";
		}
		cout << "(y)!";
		cout << "\n";


		//
		NodeS it1 = S1.top(); S1.pop();
		cout << it1->action << "       ";
		while( !S1.empty() )
		{
			NodeS it = S1.top(); S1.pop();

			cout << it->action << "         ";
		}

		cout << "(y)!";
	}
}

// for test
bool** allocateArray(int m, int n)
{
	bool** temp;
    temp = new bool*[m];
    for(int i = 0; i < m; i++)
        temp[i] = new bool[n];

    for( int i = 0; i <= m - 1; i++)
        for(int j = 0; j <= n-1; j++)
            temp[i][j]= 0;

    return temp;

}


//for debug
void printArray(int m, int n, bool** a, string arrName="noName")
{
	cout << "\n" << arrName << ":\n";
    for( int i = 0; i <= m - 1; i++)
    {
    	for( int j = 0; j <= n-1; j++)
    		cout << a[i][j] << " ";
    	cout << endl;
    }

}


// call this function from main() to start
void AStart(bool** a, int m, int n, int x1, int y1, int x2, int y2, int option, bool verboseFlag)
{
	NodeS goal = makeGoalNode(x2, y2);
	NodeS start = makeStartNode(x1, y1, goal);
    NodeS reach = NULL;

    pq_type1 pq1;
    pq_type2 pq2;
	bool** close = allocateArray(m, n);

    reach = solution(start, reach, m, n, a, close, goal, pq1, pq2, option, verboseFlag);
    trace(reach);

}

