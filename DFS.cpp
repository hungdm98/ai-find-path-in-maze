

/*******************************************************************************
** @author Đinh Mạnh Hùng                                                     **
** Phần code tìm đường đi bằng DFS (đệ quy)                                   **
**
** Cách dùng:                                                                 **
** include "data_type.h"                                                      **
** Gọi hàm bool dfs (bool** mazeMap, int width, int height, coordinate startPos, coordinate goalPos)                   **
** Hàm trả về 0 nếu không tìm thấy đường và 1 nếu tìm thấy đường              **
** Thông tin chi tiết đường đi được lưu trong vector path và visitHistory     **
** THANK YOU!                                                                 **
*******************************************************************************/

#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>

#include "data_type.h"


extern vector <coordinate> path;   // Lưu đường đi

extern vector <coordinate> visitHistory; // Lưu lịch sử thăm

int** travelCost; // Mảng 2 chiều, lưu chi phí để đến được 1 điểm

coordinate** previousCell; // Điểm trước đó dẫn đến điểm này


bool stop;

/**
* Phần đệ quy của dfs
* @param currentCell thông tin ô hiện tại đang xét
* @param currentPostition Tọa độ của ô đó
* @param mazeMap Toàn bộ mê cung
* @param width Chiều ngang mê cung
* @param height Chiều dọc mê cung
*/
void dfs_backtracking (coordinate currentPosition, bool** mazeMap, int width, int height, coordinate goalPos)
{
//  cout << currentPosition.first << " " << currentPosition.second << endl;

//  Thêm nút vào lịch sử truy cập
    if (currentPosition == goalPos)
    {
        visitHistory.push_back(currentPosition);
        stop = true;
    }

    if (!stop) visitHistory.push_back(currentPosition);
    else return;

    int currentCost = travelCost[currentPosition.first][currentPosition.second];

//  Kiểm tra các ô xung quanh có thể đi tới
//  Đi sang trái
    if (currentPosition.first > 0)
    {
//      Một ô có thể được xét nhiều lần
//      Nhưng lần xét sau phải đảm bảoz chi phí nhỏ hơn lần xét trước
        coordinate nextPosition = make_pair(currentPosition.first - 1, currentPosition.second);

//      Kiểm tra có thể tối ưu chi phí hơn hay không
        if (travelCost[nextPosition.first][nextPosition.second] > currentCost+1 && mazeMap[nextPosition.first][nextPosition.second])
        {
//          Gán chi phí mới cho đỉnh đó
            travelCost[nextPosition.first][nextPosition.second] = currentCost+1;

//          Đặt nút cha cho đỉnh đó
            previousCell[nextPosition.first][nextPosition.second] = currentPosition;

//          Gọi đệ quy
            dfs_backtracking(nextPosition, mazeMap, width, height, goalPos);
        }
    }

//  Đi sang phải
    if (currentPosition.first < height-1)
    {
//      Tương tự như trên
        coordinate nextPosition = make_pair(currentPosition.first + 1, currentPosition.second);

        if (travelCost[nextPosition.first][nextPosition.second] > currentCost+1 && mazeMap[nextPosition.first][nextPosition.second])
        {
            travelCost[nextPosition.first][nextPosition.second] = currentCost+1;
            previousCell[nextPosition.first][nextPosition.second] = currentPosition;
            dfs_backtracking(nextPosition, mazeMap, width, height, goalPos);
        }
    }

//  Đi lên trên
    if (currentPosition.second > 0)
    {
//      Tương tự như trên
        coordinate nextPosition = make_pair(currentPosition.first, currentPosition.second - 1);

        if (travelCost[nextPosition.first][nextPosition.second] > currentCost+1 && mazeMap[nextPosition.first][nextPosition.second])
        {
            travelCost[nextPosition.first][nextPosition.second] = currentCost+1;
            previousCell[nextPosition.first][nextPosition.second] = currentPosition;
            dfs_backtracking(nextPosition, mazeMap, width, height, goalPos);
        }
    }

//  Đi xuống dưới
    if (currentPosition.second < width-1)
    {
//      Tương tự như trên
        coordinate nextPosition = make_pair(currentPosition.first, currentPosition.second+1);

        if (travelCost[nextPosition.first][nextPosition.second] > currentCost+1 && mazeMap[nextPosition.first][nextPosition.second])
        {
            travelCost[nextPosition.first][nextPosition.second] = currentCost+1;
            previousCell[nextPosition.first][nextPosition.second] = currentPosition;
            dfs_backtracking(nextPosition, mazeMap, width, height, goalPos);
        }
    }
}

/**
* Tìm đường đi trong mê cung bằng DFS
* @return true nếu tìm được đường và ngược lại
* @param mazeMap Mảng 2 chiều lưu thông tin mê cung
* @param width Chiều ngang mê cung
* @param height Chiều dọc
*/
bool dfs (bool** mazeMap, int width, int height, coordinate startPos, coordinate goalPos)
{
    travelCost = init(width, height, INT_MAX);
    previousCell = init(width, height, make_pair(-1, -1));
    stop = false;

    travelCost[startPos.first][startPos.second] = 0;

//  Đệ quy
    dfs_backtracking(startPos, mazeMap, width, height, goalPos);

//  Kiểm tra có tìm được đường hay khoogn
    if (previousCell[goalPos.first][goalPos.second] == make_pair(-1, -1))
    {
        delete[] travelCost;
        delete[] previousCell;
        return false;
    }

    else
    {
//      Lấy thông tin đường đi
        coordinate tmp = goalPos;

        path.push_back(tmp);

        while (previousCell[tmp.first][tmp.second] != make_pair(-1, -1))
        {
            tmp = previousCell[tmp.first][tmp.second];
            path.push_back(tmp);
        }

//      Đường đi lưu ngược nên phải reverse lại
        reverse(path.begin(), path.end());
    }

        delete[] travelCost;
        delete[] previousCell;
        return true;
}




