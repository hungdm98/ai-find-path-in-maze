#include <iostream>
#include <vector>
#include <climits>
#include <cstdlib>
#include <ctime>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


using namespace std;

// Kiểu dữ liệu để lưu tọa độ của một đỉnh
typedef pair<int, int> coordinate;


bool** generateMazeRandomly(int width, int height, int wallPercent=30);

/**
* Tìm đường đi trong mê cung bằng DFS
* @return true nếu tìm được đường và ngược lại
* @param mazeMap Mảng 2 chiều lưu thông tin mê cung
* @param width Chiều ngang mê cung
* @param height Chiều dọc
* @param startPos Cặp tọa độ xuất phát
* @param goalPos Cặp tọa độ kết thúc
*/
bool dfs (bool** mazeMap, int width, int height, coordinate startPos, coordinate goalPos);


/**
* Tìm đường đi trong mê cung bằng BFS
* @return true nếu tìm được đường và ngược lại
* @param mazeMap Mảng 2 chiều lưu thông tin mê cung
* @param width Chiều ngang mê cung
* @param height Chiều dọc
*/
bool bfs (bool** mazeMap, int width, int height, coordinate startPos, coordinate goalPos);


bool** generateMazeDFS(int width, int height);


/**
* Hàm khởi tạo mảng 2 chiều
* @param width Chiều rộng
* @param height Chiều cao
* @param initValue Giá trị khởi tạo
*/
template <typename arrayType>
    arrayType** init(int width, int height, arrayType initValue)
    {
         //  Cấp phát bộ nhớ theo cột
        arrayType** arr = new arrayType*[height];

        //  Cấp phát bộ nhớ theo hàng
        for (int i=0; i < height; i++)
        {
            arr[i] = new arrayType[width];
        }

        //  Đặt giá trị ban đầu
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
            {
                arr[i][j] = initValue;
            }

        return arr;
    }

/**
* Hàm vẽ ma trận
*/
cv::Mat drawMaze(string mapName, int width, int height, bool** maze);
void drawPath(cv::Mat mazeMap, int width, int height, vector<coordinate> visitHistory, vector<coordinate> path);
void markPos(cv::Mat mazeMap, string mapName, int width, int height, int x, int y);
