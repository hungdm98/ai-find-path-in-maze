#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>

#include "data_type.h"
#include "AStarv3.h"

#define PADDING 5
#define MAX_RESOLUTION 650
#define COLOR_BACKGROUND Scalar(40, 32, 31)
#define COLOR_CELL Scalar(238, 222, 199)
#define COLOR_VISITED Scalar(209, 141, 27)
#define COLOR_VISITING Scalar(45, 0, 230)
#define COLOR_PATH Scalar(0, 0, 255)
using namespace cv;
using namespace std;

extern vector <coordinate> path;   // Lưu đường đi

extern vector <coordinate> visitHistory; // Lưu lịch sử thăm

/**
* Giảm kích thước ô khi bản đồ quá lớn
*/
int getCellSize(int width, int height)
{
    int cellSize = (int)(MAX_RESOLUTION/height);
    if (cellSize == 0) return 1;
    if (cellSize >= 10) return 10;
    return cellSize;
}

cv::Mat drawMaze(string mapName, int width, int height, bool** maze)
{
    int cellSize = getCellSize(width, height);
    cv::Mat img = cv::Mat(PADDING*2 + cellSize*height, PADDING*2 + cellSize*width, CV_8UC3);
    img = cv::COLOR_BACKGROUND;


    for (int i = 0; i < height; i++)
        for (int j = 0; j < width; j++)
        {
            if (maze[i][j])
            {
                int cellPosX = PADDING + cellSize*j;
                int cellPosY = PADDING + cellSize*i;
                cv::rectangle(img, cv::Rect(cellPosX, cellPosY, cellSize, cellSize), cv::COLOR_CELL, -1);
            }
        }
    cv::imshow(mapName, img);
    waitKey(0);
    return img;
}

void markPos(cv::Mat mazeMap, string mapName, int width, int height, int x, int y)
{
    int cellSize = getCellSize(width, height);
    int cellPosX = PADDING + cellSize*x;
    int cellPosY = PADDING + cellSize*y;
    cv::rectangle(mazeMap, cv::Rect(cellPosX, cellPosY, cellSize, cellSize), cv::COLOR_PATH, -1);
    cv::imshow(mapName, mazeMap);
    waitKey(0);
}


void drawPath(cv::Mat mazeMap, int width, int height, vector<coordinate> visitHistory, vector<coordinate> path)
{
    int cellSize = getCellSize(width, height);
    for (int i = 0; i < visitHistory.size(); i++)
    {
        coordinate tmp = visitHistory.at(i);
        int cellPosX = PADDING + cellSize*tmp.second;
        int cellPosY = PADDING + cellSize*tmp.first;
        cv::rectangle(mazeMap, cv::Rect(cellPosX, cellPosY, cellSize, cellSize), cv::COLOR_VISITING, -1);
        cv::imshow("map 1", mazeMap);
        waitKey(10);
        cv::rectangle(mazeMap, cv::Rect(cellPosX, cellPosY, cellSize, cellSize), cv::COLOR_VISITED, -1);

    }
     for (int i = 0; i < path.size(); i++)
    {
        coordinate tmp = path.at(i);
        int cellPosX = PADDING + cellSize*tmp.second;
        int cellPosY = PADDING + cellSize*tmp.first;
        cv::rectangle(mazeMap, cv::Rect(cellPosX, cellPosY, cellSize, cellSize), cv::COLOR_PATH, -1);
        cv::imshow("map 1", mazeMap);
        waitKey(10);
        cv::rectangle(mazeMap, cv::Rect(cellPosX, cellPosY, cellSize, cellSize), cv::COLOR_PATH, -1);

    }
         cv::imshow("map 1", mazeMap);

        waitKey(0);
}
/**
int main(int argc, char *argv[])


{
    int width = 30;
    int height = 20;
    bool** maze;

    maze = generateMazeDFS(width, height);

    int cellSize = getCellSize(width, height);
    cv::Mat img = cv::Mat(PADDING*2 + cellSize*height, PADDING*2 + cellSize*width, CV_8UC3);
    img = cv::COLOR_BACKGROUND;


    for (int i = 0; i < height; i++)
        for (int j = 0; j < width; j++)
        {
            if (maze[i][j])
            {
                int cellPosX = PADDING + cellSize*j;
                int cellPosY = PADDING + cellSize*i;
                cv::rectangle(img, cv::Rect(cellPosX, cellPosY, cellSize, cellSize), cv::COLOR_CELL, -1);
            }
        }
    cv::imshow("Image", img);
    waitKey(0);

    // Tìm đ
//    AStart(maze, height, width, 1, 1, height-1, width-1, 1, 5, 1);
    bfs(maze, width, height, make_pair(height-1, width-1), make_pair(0, 0));

    for (int i = 0; i < visitHistory.size(); i++)
    {
        coordinate tmp = visitHistory.at(i);
        int cellPosX = PADDING + cellSize*tmp.second;
        int cellPosY = PADDING + cellSize*tmp.first;
        cv::rectangle(img, cv::Rect(cellPosX, cellPosY, cellSize, cellSize), cv::COLOR_VISITING, -1);
        cv::imshow("Image", img);
        waitKey(10);
        cv::rectangle(img, cv::Rect(cellPosX, cellPosY, cellSize, cellSize), cv::COLOR_VISITED, -1);

    }
     for (int i = 0; i < path.size(); i++)
    {
        coordinate tmp = path.at(i);
        int cellPosX = PADDING + cellSize*tmp.second;
        int cellPosY = PADDING + cellSize*tmp.first;
        cv::rectangle(img, cv::Rect(cellPosX, cellPosY, cellSize, cellSize), cv::COLOR_PATH, -1);
        cv::imshow("Image", img);
        waitKey(10);
        cv::rectangle(img, cv::Rect(cellPosX, cellPosY, cellSize, cellSize), cv::COLOR_PATH, -1);

    }
         cv::imshow("Image", img);

        waitKey(0);


}



**/
