#ifndef ASTART_H_INCLUDED
#define ASTART_H_INCLUDED

#include <stdio.h>
#include <iostream>
#include <queue>
#include <stack>
#include <vector>
#include <cmath>
#include <string>
using namespace std;

// each state is a node
typedef struct Node{
    int x, y;       // coordinates, x - row, y - col
    Node* parent;   // point to its parent --> tracing path
    string action;  // what action made this node from its parent

    int g, h2, f2;  // cost
    float h1, f1;   // cost too
} *NodeS;


// this code to make order of priority_queue
struct MyComparator1 {
    bool operator() (NodeS arg1, NodeS arg2) {
        return arg1->f1 > arg2->f1;
    }
};


// this code too, but use f2(x) to make order
struct MyComparator2 {
    bool operator() (NodeS arg1, NodeS arg2) {
        return arg1->f2 > arg2->f2;
    }
};


// for short naming
typedef priority_queue<NodeS, vector<NodeS>, MyComparator1> pq_type1;
typedef priority_queue<NodeS, vector<NodeS>, MyComparator2> pq_type2;


void AStart(bool** a, int m, int n, int x1, int y1, int x2, int y2, int option = 2, bool verboseFlag = false);


#endif // ASTART_H_INCLUDED

